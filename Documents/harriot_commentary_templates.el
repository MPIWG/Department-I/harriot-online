;;; harriot_commentary_templates.el --- contains templates to quickly insert commentaries and translations

;; Copyright (C) 2012  Klaus Thoden

;; Author: Klaus Thoden <kthoden@mpiwg-berlin.mpg.de>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides definitions for macros to quickly insert
;; Commentaries and Translations into Harriot's transcriptions. As the
;; result will at one point be fed into an annotation system, date and
;; username are also inserted.
;; 
;; As of now, no keyboard shortcuts have been defined.
;;
;; Please put this file somewhere in your load path and activate it
;; using \(require 'harriot_commentary_templates).

;;; Code:

;; defining two variables here for more control about name to be
;; displayed and format of date
(setq harriot/userName user-login-name)
(setq harriot/dateFormat "%Y-%m-%d")

;; Six function definitions. Right now, they use the minibuffer as
;; input buffer which is not optimal if input strings are getting
;; longer. As a workaround, type just the beginning of the text, press
;; enter and continue typing in the main buffer.

(defun harriot/page-commentary (commString)
  "Add a tag for page commentary. This is the COMMSTRING which you write in the minibuffer."
  (interactive "MEnter commentary (use C-q C-j to enter a linebreak): ")
  (insert (concat "<c date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<p>\n"
                  "<s xml:space=\"preserve\">" commString "</s>\n"
                  "</p>\n"
                  "</c>\n"
                  )))

(defun harriot/paragraph-commentary (pCommString)
  "Add a tag for paragraph commentary. The PCOMMSTRING is written in the minibuffer."
  (interactive "MEnter commentary (use C-q C-j to enter a linebreak): ")
  (insert (concat "<pc date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<s xml:space=\"preserve\">" pCommString "</s>\n"
                  "</pc>\n"
                  )))

(defun harriot/sentence-commentary (sCommString)
  "Add a tag for sentence commentary. Write the SCOMMSTRING in the minibuffer."
  (interactive "MEnter commentary (use C-q C-j to enter a linebreak): ")
  (insert (concat "<sc date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<s xml:space=\"preserve\">" sCommString "</s>\n"
                  "</sc>\n"
                  )))

(defun harriot/sentence-translation (trString)
  "Add a tag for sentence translation. The TRSTRING is to be written in the minibuffer."
  (interactive "MEnter translation (use C-q C-j to enter a linebreak): ")
  (insert (concat "<t date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<s xml:space=\"preserve\">" trString "</s>\n"
                  "</t>\n"
                  )))

(defun harriot/blank-page ()
  "Inserts a commentary for a blank page."
  (insert (concat "<c date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<p>\n"
                  "<s xml:space=\"preserve\">Blank page</s>\n"
                  "</p>\n"
                  "</c>\n"
                  )))

(defun harriot/rough-sketch ()
  "Adds a commentary text block for a page containing rough sketch"
  (insert (concat "<c date=\""(format-time-string harriot/dateFormat) "\" user=\""harriot/userName"\">\n"
                  "<p>\n"
                  "<s xml:space=\"preserve\">Rough sketch</s>\n"
                  "</p>\n"
                  "</c>\n"
                  )))

(provide 'harriot_commentary_templates)
;;; harriot_commentary_templates.el ends here
