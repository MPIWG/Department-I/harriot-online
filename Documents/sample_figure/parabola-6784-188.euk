% Diagram of parabola on 6784, fol. 188r

box -1,-1, 10,10

origin = point(0,0)
axis = line(origin,0:)


% Focus
parameter = 2
a = point(2*parameter,0)
o = origin
x = point(parameter,4*parameter)

pvertex = point(parameter,0)
b = pvertex
directrix = line(origin, 90:)
pb = parabola(a,directrix)


g = element(intersection(perpendicular(axis,a),pb),1)

% Arbitrary point c

c = point(b.a, 1/3)
d = element(intersection(perpendicular(axis,c),pb),1)

draw
	axis; directrix
	a; x
	pb -200: 0:
	b.x dashed
	a.g
	c.d
	g; d
	d.a
end

% Tweaks in label separation added to account for ascenders in letters!

label "Georgia-Italic-11"
	a -90: 1.1
	b -90:
	c -90: 1.1
	o 225:
	x 180:
	g 90:
	d 90:
end

% The diagram so far follows the proof of Harriot's first lemma. Comment out the rest of the file to generate this first stage diagram. The remainder of the file completes the diagram as found in the manuscript.

% First, the additional points on the axis, and their ordinates to the parabola.

e = point(b.a, 2/3)
f = element(intersection(perpendicular(axis,e),pb),1)

% Although Harriot takes arbitrary points on the axis, he spaces them evenly in his diagram, so we will too.

stp = length(a.e)

h = point(abscissa(a)+stp,0)
i = element(intersection(perpendicular(axis,h),pb),1)


k = point(abscissa(a)+2*stp,0)
l = element(intersection(perpendicular(axis,k),pb),1)

draw
	f; i; l
	e.f; h.i; k.l
	f.a; i.a; l.a
end

label "Georgia-Italic-11"
      e -90: 1.1
      h -90:
      k -90:
      f 90:
      i 90:
      l 90:
end

% Next, the perpendiculars of all of these points on the parabola to the directrix

p = projection(d, directrix)
q = projection(f, directrix)
r = projection(g, directrix)
s = projection(i, directrix)
t = projection(l, directrix)

draw
	d.p
	f.q
	g.r
	i.s
	l.t
end

label "Georgia-Italic-11"
      p 180:
      q 180:
      r 180:
      s 180:
      t 180:
end

% And, finally, the circular arcs

m = reflection(e,line(b,x))
n = reflection(c,line(b,x))

CIRCm = circle(a,length(a.m))
CIRCn = circle(a,length(a.n))


draw dotted
	CIRCm (180-angle(f,a,m)): 180:
	CIRCn (180-angle(d,a,n)): 180:
end

label "Georgia-Italic-11"
      m -90: 1.1
      n -90: 1.1
end
